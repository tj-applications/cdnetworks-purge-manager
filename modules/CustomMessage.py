"""
Tamas Juhasz Mansion Gibraltar 2020
Custom Message Python Module

Call it with the arguments msg, severity
msg: message
severity: message severity (0 - Info, 1 - Warning, 2 - Error, 3 - Debug)

Returns the message and it's severity
"""
from enum import Enum


class Severity(Enum):
    """
    Enumerates the severity
    :param Enum: Enumeration to work with
    """
    INFO = 0
    WARNING = 1
    ERROR = 2
    DEBUG = 3


class CustomMessage:
    """
    Creates the CustomMessage object with message and severity
    :return: message text
    :return: message severity
    """
    def __init__(self, msg, severity):
        """
        Takes the parameters
        :param msg: Message text
        :param severity: Message severity
        """
        super().__init__()

        self.message = msg
        self.severity = severity

    def get_message(self):
        """
        Returns the text
        :return: message text
        """
        return self.message

    def get_severity(self):
        """
        Returns the severity
        :return: message severity
        """
        return self.severity
