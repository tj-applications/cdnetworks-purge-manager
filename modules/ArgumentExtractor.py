"""
Argument extractor Python module for CDNetwork Manager
"""
from argparse import ArgumentParser
from re import match


def args_extractor():
    """
    Argument extractor
    :return: arguments
    """
    parser = ArgumentParser()
    parser.add_argument('-u', '--username', nargs='?', required=True, help="Username")
    parser.add_argument('-p', '--api_key', nargs='?', required=True, help="Api Key")
    parser.add_argument('-m', '--method', nargs='?', default="purgeurl", help="API Method")
    parser.add_argument('-r', '--paths', nargs='+', required=False, help="Paths to Expire")
    parser.add_argument('-f', '--files', nargs='+', required=False, help="Files to Expire")
    parser.add_argument('-i', '--purgeid', nargs='?', required=False, help="ID of the Purge Job")
    parser.add_argument('-n', '--noverify', action='store_false', help="Do not verify SSL")
    parser.add_argument('-d', '--debug', action='store_true', help="Enable debug logging")
    args = vars(parser.parse_args())

    if args['method'] != "purgeurl" and args['method'] != "purgestatus" and args['method'] != "quota" and \
            args['method'] != "nagios":
        raise ArgumentError("Unsupported method! The following methods are supported: purgeurl, purgestatus, quota and "
                            "nagios")
    if args['method'] == "purgeurl" and (not args['files'] and not args['paths']):
        raise ArgumentError("--files (-f) argument or --paths (-r) is necessary for 'purgeurl' method")
    if args['method'] == "purgestatus" and not args['purgeid']:
        raise ArgumentError("-i or --purgeid argument is necessary for 'purgestatus' method")
    if args['files']:
        for file in args['files']:
            if not match("(http://|https://)", file):
                raise ArgumentError("URL " + file + " should start with http:// or https://")
    if args['paths']:
        for path in args['paths']:
            if not match("(http://|https://)", path):
                raise ArgumentError("Path " + path + " should start with http:// or https://")
    return args


class ArgumentError(Exception):
    """ArgumentError error class. Message returned"""
    def __init__(self, message):
        Exception.__init__(self, message)
        self.message = message
