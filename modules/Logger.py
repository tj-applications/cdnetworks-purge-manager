"""
Log writing/printing module
"""
import datetime
from modules.CustomMessage import CustomMessage


class Logger:
    """
    Main class
    """
    def __init__(self, debug=False, logfile=''):
        """
        Takes the parameters
        :param debug: Sets logging debug mode
        :param logfile: Log file to write the logs into
        """
        super().__init__()

        self.debug = debug
        self.logfile = logfile

    def send_log(self, message):
        """
        Decides if the log should be written into file, or printed to console
        :param message: Log message
        :return: N/A
        """
        if isinstance(message, CustomMessage):
            self.write_custom_log(message)
        else:
            self.print_log(message)

    @staticmethod
    def print_log(message):
        """
        Prints the log to console
        :param message: Log message
        :return: N/A
        """
        message = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S") + " - " + message
        print(message)

    # Append Text to Logs Textfield and Log File
    def write_custom_log(self, text):
        """
        Writes the log into the log file
        :param text: Log message
        :return: N/A
        """
        if isinstance(text, CustomMessage):
            if not self.debug:
                if text.get_severity().name == 'DEBUG':
                    return
            text = (text.get_severity().name + " - " + text.get_message())
        message = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S") + " - " + text
        if self.logfile != '' and text != '':
            try:
                with open(self.logfile, 'a', encoding="utf8") as log_to_write:
                    log_to_write.write(message + '\n')
            except IOError as io_error:
                raise LogFileError from io_error
        else:
            print(message)


class LogFileError(Exception):
    """ArgumentError error class. Message returned"""
    def __init__(self, message):
        Exception.__init__(self, message)
        self.message = message
