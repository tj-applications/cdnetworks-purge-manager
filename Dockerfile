FROM alpine:latest

COPY ./binary/CDNetworksManager-Alpine_x86_64 /CDNetworksManager/CDNetworksManager
RUN chmod a+x /CDNetworksManager/*

ENTRYPOINT ["/CDNetworksManager/CDNetworksManager"]