CDNetworks Manager for CDNetworks API v2<br>
<br>
Usage:<br>
-u, --username - CDNetwork API username<br>
-p, --api_key - CDNetworks API key<br>
-m, --method - API call method<br>
-f, --files - List of files to call the method against (Separated with space)<br>
-r, --paths - List of paths to call the method against (Separated with space)<br>
-i, --purgeid - ID of purge request for status check (Mandatory for 'purgestatus' method)<br>
-n, --noverify - Do not verify SSL certificate<br>
-d, --debug - Enable Debug mode<br>

CDNetworksManager.py -u {username} -p {api_key} -n -m {method} -r {URL1} {URL2}<br>
<br>
Methods:
- purgeurl (default) - Purge the selected URLs (Accepts list of URLs)
- purgestatus - Query the status of a requested purge<br>
- quota - Get query quota

Examples:<br>
```
CDNetworksManager -u {username} -p {api_token} -d -n -f http://cache.download.it.casino.com/download/casino/client_update_urls_preview.php http://cache.download.mansioncasino.com/casinoclientconf.php -r http://cachedownloadit.casino.com/download/casino/
CDNetworksManager -u {username} -p {api_token} -d -n -m purgestatus -i c82e7b1c54474b5a96eae797681e082b
```

Docker Image Usage:
```
docker run registry.mansion.com:443/devops/applications/python-applications/cdnetworks-manager:latest -u {username} -p {api_token} -d -n -f http://cache.download.it.casino.com/download/casino/client_update_urls_preview.php http://cache.download.mansioncasino.com/casinoclientconf.php -r http://cachedownloadit.casino.com/download/casino/
docker run registry.mansion.com:443/devops/applications/python-applications/cdnetworks-manager:latest -u {username} -p {api_token} -d -n -m purgestatus -i c82e7b1c54474b5a96eae797681e082b
```

Download Latest Release:
- [CentOS7](https://gitlab.mansion.com/devops/applications/python-applications/cdnetworks-manager/-/blob/master/binary/CDNetworksManager-CentOS7)
- [Ubuntu2004](https://gitlab.mansion.com/devops/applications/python-applications/cdnetworks-manager/-/blob/master/binary/CDNetworksManager-Ubuntu2004)
- [AlpineLinux](https://gitlab.mansion.com/devops/applications/python-applications/cdnetworks-manager/-/blob/master/binary/CDNetworksManager-Alpine)
