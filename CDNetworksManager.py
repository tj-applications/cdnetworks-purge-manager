"""
CDNetwork Manager - Tamas Juhasz 2020 @ Mansion Gibraltar
Usage:
-u, --username - CDNetwork API username
-p, --api_key - CDNetworks API key
-m, --method - API call method
-f, --files - Files to call the method against (Multiple items can be added)
-r, --paths - Paths to call the method against (Multiple items can be added)
-i, --purgeid - ID of purge request for status check (Mandatory for 'purgestatus' method)
-n, --noverify - Do not verify SSL certificate
-d, --debug - Enable Debug mode

CDNetworksManager.py -u {username} -p {api_key} -n -m {method} -r {URL1} {URL2}

Methods:
- purgeurl (default) - Purge the selected URLs (Accepts list of URLs)
- purgestatus - Query the status of a requested purge
- quota - Get query quota
"""
import json
import sys
from modules.ArgumentExtractor import ArgumentError, args_extractor
from modules.Logger import Logger
from modules.CustomMessage import CustomMessage, Severity
from modules.OpenAPI import OpenApi, OpenAPIError


class CallAPI:
    """
    API Call Method
    - get_auth: Generate the authentication strings
    - purge_urls: Purge URLs or Paths on CDNetworks system
    - get_purge_status: Check the status of running purge request
    """
    def __init__(self, arguments):
        self.logger = Logger(arguments['debug']).send_log

    @staticmethod
    def get_auth(arguments):
        """
        Check GMT Time and requests the generation of authentication string
        :return: GMT Time and OpenAPI authentication string
        """
        gmt_date = OpenApi.get_date()
        return gmt_date, OpenApi.get_auth(arguments['username'], arguments['api_key'], gmt_date)

    def purge_urls(self, api_url, api_headers, arguments):
        """
        Sends the purge request to CDNetworks and calls its status check
        :param api_url: API endpoint URL
        :param api_headers: HTTP headers to send to API
        :param arguments: Arguments from CLI
        :return: N/A
        """
        purge_api_url = api_url + "/purge/ItemIdReceiver"
        request_body = {
            "urls": arguments['files'],
            "urlAction": "Delete",
            "dirs": arguments['paths'],
            "dirAction": "Expire"
        }
        self.logger(CustomMessage("Purging URLs", Severity.INFO))
        if arguments['files']:
            self.logger(CustomMessage("Requested URLs: " + ', '.join(arguments['files']), Severity.DEBUG))
        if arguments['paths']:
            self.logger(CustomMessage("Requested Directories: " + ', '.join(arguments['paths']), Severity.DEBUG))
        call = OpenApi().send_request(purge_api_url, "POST", json.dumps(request_body), api_headers, arguments['noverify'])
        response = json.loads(call.text)
        if 'Code' in response:
            if response['Code'] == 0:
                if response['Message']:
                    message = response['Message'].rstrip("\r\n")
                    raise RuntimeException(message)
                if not response['Message']:
                    raise RuntimeException("Reason: " + call.reason + ", Text: " + call.text)
            else:
                self.logger(CustomMessage("Purge Initiated - " + response['Message'], Severity.INFO))
                if response['Code'] == 1:
                    self.logger(CustomMessage("Item ID: " + response['itemId'], Severity.DEBUG))
                    self.get_purge_status(api_url, api_headers, arguments, response['itemId'])
                    self.get_purge_quota(api_url, api_headers, arguments)
        elif 'status' in response:
            if response['status'] == 0:
                raise RuntimeException(response['result'])

    def get_purge_status(self, api_url, api_headers, arguments, purge_id):
        """
        Checks the status of a running purge
        :param api_url: API endpoint URL
        :param api_headers: HTTP headers to send to API
        :param purge_id: ID of the purge request returned by purge_urls()
        :param arguments: Arguments from CLI
        :return: N/A
        """
        api_url = api_url + "/purge/ItemIdQuery"
        request_body = {
           "itemId": purge_id,
        }
        self.logger(CustomMessage("Checking purge status", Severity.INFO))
        call = OpenApi().send_request(api_url, "POST", json.dumps(request_body), api_headers, arguments['noverify'])
        response = json.loads(call.text)
        result_details = response['resultDetail']
        if call.status_code == 200:
            if arguments['debug']:
                for url in result_details:
                    self.logger(CustomMessage("URL: " + url['url'], Severity.DEBUG))
                    self.logger(CustomMessage("Create Time: " + url['createTime'] + " China Standard Time",
                                              Severity.DEBUG))
                    self.logger(CustomMessage("Status: " + url['status'], Severity.DEBUG))
            self.logger(CustomMessage("Purge outcome: " + response['Message'], Severity.INFO))
        if call.status_code != 200:
            raise RuntimeException(response['result'])

    def get_purge_quota(self, api_url, api_headers, arguments):
        """
        Checks the remaining purge quota for the account used to run the purge
        :param api_url: API endpoint URL
        :param api_headers: HTTP headers to send to API
        :param arguments: Arguments from CLI
        :return: N/A
        """
        api_url = api_url + "/upperQuery?type=purge"
        if arguments['method'] != "nagios":
            self.logger(CustomMessage("Checking query quota", Severity.INFO))
        call = OpenApi().send_request(api_url, "GET", "",  api_headers, arguments['noverify'])
        response = json.loads(call.text)
        if 'code' in response:
            if response['code'] == 1:
                raise RuntimeException(response['message'] + " from supplier " + response['supplier'])
            if response['code'] != 1:
                if arguments["method"] != "nagios":
                    self.logger(CustomMessage("URLs remain: " + str(response['urlRemain']), Severity.INFO))
                    self.logger(CustomMessage("Paths remain: " + str(response['dirRemain']), Severity.INFO))
                else:
                    if response['urlRemain'] < 1000 or response['dirRemain'] < 100:
                        status = "WARNING"
                        exit_code = 1
                    elif response['urlRemain'] < 100 or response['dirRemain'] < 10:
                        status = "CRITICAL"
                        exit_code = 2
                    else:
                        status = "OK"
                        exit_code = 0
                    print(status + " - URLs remain: " + str(response['urlRemain']) + ", Paths remain: " +
                          str(response['dirRemain']) + "|url=" + str(response['urlRemain']) + ";1000;100;0;5000 "
                          "path=" + str(response['dirRemain']) + ";100;10;0;500")
                    sys.exit(exit_code)
        elif 'status' in response:
            if response['status'] == 0:
                raise RuntimeException(response['result'])


class RuntimeException(Exception):
    """Runtime error class. Message will be logged"""
    def __init__(self, message):
        Exception.__init__(self, message)
        self.message = message


if __name__ == '__main__':
    try:
        # Set Variables
        OPENAPI_URL = "https://api.cdnetworks.com/ccm"
        # Parse Arguments
        args = args_extractor()
        if not args['noverify']:
            if args['method'] != "nagios":
                Logger(False).send_log(CustomMessage("SSL verification is disabled", Severity.WARNING))
        # Get Auth Requirements
        date, auth_str = CallAPI(args).get_auth(args)
        headers = OpenApi().create_header('application/json', auth_str, date)
        if args["method"] == "purgeurl":
            CallAPI(args).purge_urls(OPENAPI_URL, headers, args)
        if args["method"] == "purgestatus":
            CallAPI(args).get_purge_status(OPENAPI_URL, headers, args, args["purgeid"])
        if args["method"] == "quota" or args["method"] == "nagios":
            CallAPI(args).get_purge_quota(OPENAPI_URL, headers, args)

    except KeyboardInterrupt:
        Logger().send_log(CustomMessage("Script interrupted by user", Severity.INFO))
        sys.exit(0)
    except ArgumentError as argument_exception:
        Logger().send_log(CustomMessage(argument_exception.message, Severity.ERROR))
        sys.exit(1)
    except OpenAPIError as openapi_exception:
        Logger().send_log(CustomMessage(openapi_exception.message, Severity.ERROR))
        sys.exit(1)
    except RuntimeException as runtime_exception:
        Logger().send_log(CustomMessage(runtime_exception.message, Severity.ERROR))
        sys.exit(1)
